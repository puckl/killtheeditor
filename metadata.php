<?php
/*    Please retain this copyright header in all versions of the software
 *
 *    Copyright (C) Josef A. Puckl | eComStyle.de
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see {http://www.gnu.org/licenses/}.
 */

$sMetadataVersion = '2.0';
$aModule          = [
    'id'            => 'ecs_killtheeditor',
    'title'         => '<strong style="color:#04B431;">e</strong><strong>ComStyle.de</strong>:  <i>KillTheEditor</i>',
    'description'   => 'Deaktiviert den WYSIWYG-Editor mit nur einen Klick beim jeweiligen Textfeld im Shop.',
    'version'       => '1.0.4',
    'thumbnail'     => 'ecs.png',
    'author'        => '<strong style="font-size: 17px;color:#04B431;">e</strong><strong style="font-size: 16px;">ComStyle.de</strong>',
    'email'         => 'info@ecomstyle.de',
    'url'           => 'https://ecomstyle.de',
    'extend' => [
        \OxidEsales\Eshop\Application\Controller\TextEditorHandler::class       => Ecs\KillTheEditor\Controller\TextEditorHandler::class,
        \OxidEsales\Eshop\Application\Controller\Admin\ContentMain::class       => Ecs\KillTheEditor\Controller\Admin\KillTheEditor::class,
        \OxidEsales\Eshop\Application\Controller\Admin\ArticleMain::class       => Ecs\KillTheEditor\Controller\Admin\KillTheEditor::class,
        \OxidEsales\Eshop\Application\Controller\Admin\PaymentMain::class       => Ecs\KillTheEditor\Controller\Admin\KillTheEditor::class,
        \OxidEsales\Eshop\Application\Controller\Admin\CategoryText::class      => Ecs\KillTheEditor\Controller\Admin\KillTheEditor::class,
        \OxidEsales\Eshop\Application\Controller\Admin\NewsletterMain::class    => Ecs\KillTheEditor\Controller\Admin\KillTheEditor::class,
        \OxidEsales\Eshop\Application\Controller\Admin\NewsText::class          => Ecs\KillTheEditor\Controller\Admin\KillTheEditor::class,
        \OxidEsales\Eshop\Application\Controller\Admin\ActionsMain::class       => Ecs\KillTheEditor\Controller\Admin\KillTheEditor::class,
        \OxidEsales\Eshop\Application\Controller\Admin\AdminlinksMain::class    => Ecs\KillTheEditor\Controller\Admin\KillTheEditor::class,
    ],
    'settings' => [
        ['group' => 'ecs_main', 'name' => 'ecs_killtheeditor', 'type' => 'arr', 'value' => []],
    ],
    'events' => array(
        'onActivate'   => 'Ecs\KillTheEditor\Core\Events::onActivate',
        'onDeactivate' => 'Ecs\KillTheEditor\Core\Events::onDeactivate',
    ),
];
