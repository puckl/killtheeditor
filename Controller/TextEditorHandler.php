<?php
/*    Please retain this copyright header in all versions of the software
 *
 *    Copyright (C) Josef A. Puckl | eComStyle.de
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see {http://www.gnu.org/licenses/}.
 */

namespace Ecs\KillTheEditor\Controller;

use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Core\DatabaseProvider;

class TextEditorHandler extends TextEditorHandler_parent
{

    public function renderTextEditor($width, $height, $objectValue, $fieldName)
    {
        $oConfig   = Registry::getConfig();
        $oLang     = Registry::getLang();
        $oxid      = $oConfig->getRequestParameter("oxid");
        $aKillThem = $oConfig->getConfigParam('ecs_killtheeditor');

        $sInputFieldOn  = $this->_getTheInput($oLang->translateString("ecs_editoron"));
        $sInputFieldOff = $this->_getTheInput($oLang->translateString("ecs_editoroff"));
        if (!$oxid or $oxid == '-1') {
            return parent::renderTextEditor($width, $height, $objectValue, $fieldName);
        }
        if ($fieldName == 'oxcontents__oxcontent') {
            if ($isPlain = $this->_isCmsIdentPlain($oxid)) {
                $sInputFieldOn = $sInputFieldOff = null;
            }
        }
        if ($isPlain or in_array($oxid, $aKillThem)) {
            return $this->renderPlainTextEditor($width, $height, $objectValue, $fieldName) . $sInputFieldOn;
        }
        return parent::renderTextEditor($width, $height, $objectValue, $fieldName) . $sInputFieldOff;
    }

    protected function _getTheInput($onoff = null)
    {
        $oLang = Registry::getLang();
        $input = '<p>';
        $input .= '<input type="submit" title="' . $oLang->translateString("GENERAL_SAVE") . '" style="border-color: #464646; padding: 1px 10px;" onClick="Javascript:document.myedit.fnc.value=\'save\'"" value="' . $oLang->translateString("GENERAL_SAVE") . '">';
        $input .= '<input type="submit" title="KillTheEditor is proudly powered by eComStyle.de" style="color: #7a7a7a; padding: 1px 10px;" onClick="Javascript:document.myedit.fnc.value=\'KillTheEditor\'"" value="' . $onoff . '">';
        $input .= '</p>';
        return $input;
    }

    protected function _isCmsIdentPlain($oxid)
    {
        $sSelect = "SELECT `oxloadid` FROM `oxcontents` WHERE `oxid` = '" . $oxid . "'";
        try
        {
            $CmsIdent = DatabaseProvider::getDb()->getOne($sSelect, false, false);
        } catch (\Exception $oException) {
        }
        if (strpos($CmsIdent, 'plain')) {
            return true;
        }
        return false;
    }
}
