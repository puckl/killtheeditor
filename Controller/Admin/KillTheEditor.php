<?php
/*    Please retain this copyright header in all versions of the software
 *
 *    Copyright (C) Josef A. Puckl | eComStyle.de
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see {http://www.gnu.org/licenses/}.
 */

namespace Ecs\KillTheEditor\Controller\Admin;

use \OxidEsales\Eshop\Core\Registry;

class KillTheEditor extends KillTheEditor_parent
{
    public function KillTheEditor($oxid = null)
    {
        $this->save();
        $sModulename = 'module:ecs_killtheeditor';
        $oConfig     = Registry::getConfig();
        $sShopId     = $oConfig->getShopId();
        $aKillThem   = $oConfig->getConfigParam('ecs_killtheeditor');
        $oxid        = $this->getEditObjectId();
        if (!in_array($oxid, $aKillThem)) {
            $aKillThem[] = $oxid;
            $aKillThem   = array_values($aKillThem);
        } else {
            unset($aKillThem[array_search($oxid, $aKillThem)]);
            $aKillThem = array_values($aKillThem);
        }
        $oConfig->saveShopConfVar('arr', 'ecs_killtheeditor', $aKillThem, $sShopId, $sModulename);
    }
}
